# Lab 12 - 3D Printing and Pixhawk Firmware Update (20 points)

[TOC]

---

## 1. Objectives

The objective of this lab is to print the GPS and flight controller mounts from last week. Additionally, we will update the Pixhwak flight controllers with the latest firmware.

---

## 2. Overview

In lab 11 we designed a flight controller mount to preemptively mitigate vibrations from motors and propellers as well as a GPS mount to elevate the GPS (and accompanying magnetometer) away from potential interference sources including the wiring and motors. This week we will bring those prints to life by printing them on the 3D printers.

We will also test the flight controllers, reset the parameters, and update the firmware to ensure that we are working with a set of default parameters from which to make adjustments and optimizations to. 

## 3. 3D Print Design Review/Presentation (5 points)

Printing can be a time consuming process, so to avoid potential, unnecessary reprints, you will present your design to the instructor before preparing to send it to the printer. In addition to an efficient print, the team (across both lab section) with the best vibration damping (as calculated by the instructor using the default tools available in QGroundControl) will receive a 5 point bonus on an assignment of their choosing. Prepare to describe the following:

1. How will your GPS mount and FC mount designs mount to the UAS?

2. Do you have the correct cut outs for the GPS wire? Does the connector fit?

3. Are the cable length sufficient to accommodate your GPS/FC mount designs?

4. What method are your using for vibration damping on the FC mount?

5. How will the GPS mount to the GPS mount? The FC to the FC mount?

Only move on to step 4 once you receive instructor approval of your design.

## 4. 3D Printing Preparation (12 points)

### 4.1. Download/Setup Cura LulzBot Edition Slicing Program

Recall that the 3D printer is not a "smart" device in that it does not make machine-level decisions, but instead follows a very precise set of instructions known as [G-code](https://all3dp.com/2/3d-printer-g-code-commands-list-tutorial/) which direct a the functional set of servos that make up the 3D printer. We use a program called a slicer to generate G-code for our specific machine, the [LulzBot Taz 6](https://legacy.lulzbot.com/store/printers/lulzbot-taz-6). 

Download the [Cura LulzBot Edition program](https://learn.lulzbot.com/support/cura) for your corresponding operating system. Set up your software according to the [Taz 6 operation manual](https://gitlab.com/purdue-uas/at21900/course-material/-/blob/main/resources/LulzBot_Taz6_Manual.pdf). 

### 4.2. Export CAD

In order to upload your design into Cura, the 3D model will first need to be exported from your CAD program into one of the file types below. In FreeCAD this is done by selecting the model body from the Combo View, then selecting File > Export, and saving the file as an *.obj file. (*[Figure 1](#fig1)*)

*<a name="fig1">![lab12_exportCAD](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab12_exportCAD.gif) <br>Figure 1: Export 3D model from FreeCAD.</a>*

#### 3D models

- 3MF File (.3mf)
- AMF File (.amf)
- COLLADA Digital Asset Exchange (.dae)
- Compressed COLLADA Digital Asset Exchange (.zae)
- Open Compressed Triangle Mesh (.ctm)
- STL file (.stl)
- Stanford Triangle Format (.ply)
- **Wavefront OBJ File (.obj)** (FreeCAD default)
- X3D file (.x3d)
- glTF Binary (.glb)
- glTF Embedded JSON (.gltf)

#### Images

- BMP image (.bmp)
- GIF image (.gif)
- JPEG image (.jpeg)
- JPG image (.jpg)
- PNG Image (.png)

#### Gcode

- Compressed G-code file (.gz)
- G file (.g)
- G-code file (.gcode)

### 4.3. Import Model into Cura

We now need to import the 3D model from step 4.2. into Cura to begin the slicing process. From the File menu, select Open File(s) and select the appropriate file. This will open the file into the window showing the approximate size relative to the printing plate. (*[Figure 2](#fig2)*)

*<a name="fig2">![lab12_importModel](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab12_importModel.gif) <br>Figure 2: Import model into Cura.</a>*

### 4.4. Optimize for Print

Recall from [Lecture 9.2](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/week9/lecture9.2/presentation9.2.pptx?inline=false) that there are very important considerations when converting a 3D model to a 3D print. Review these considerations and record potential issues below and possible ways to mitigate the issues (consider issues like print/model orientation, bridging, overhang, etc.):

`possible considerations for 3D printing`

### 4.5. Adjust Printer Settings

The Cura defaults generally work very well for most prints. However, if you have bridging, overhang, or other regions that require support this will need to be generated in the G-code before sending to the printer. Change/verify the following settings:

1. Material:     PLA (Village Plastic)
2. Profile:     High Detail - 0.140mm
3. Generate Support (under Support heading):    Checked \*if required for design.

Additionally, record the following parameter values and describe what it does:

1. Infill Density: `blank`
2. Material > Print Temperature: `blank`
3. Material > Diameter: `blank`
4. Build Plate Adhesion > Build Plate Adhesion Type: `blank`

Finally, save the Cura project (\*.3mf) with your team name in the file name. This will be turned in with the lab assignment as well as emailed directly to the instructor for printing. 

## 5. Update Pixhawk Firmware (3 points)

The Pixhawk is capable of running two main lineages of firmware, [PX4](https://px4.io) which is tailored to the commercial stream, and [ArduPilot](https://ardupilot.org) which is tailored to the community stream. Typically, the ArduPilot lineage is most up to date with experimental features as all components of it are community contributed. For this class, we will use the ArduPilot firmware. 

### 5.1. Download/Install Ground Control Software

There are several options when considering a ground control software to run on your computer and/or phone including [MissionPlanner](https://ardupilot.org/planner/index.html#home), [APM Planner 2.0](https://ardupilot.org/planner2/), [QGroundControl](http://qgroundcontrol.com), and several more. For this class, we will use QGroundControl as it runs on Windows, MacOS, Linux, as well as mobile giving us the flexibility to use any platform to interface with the UAS. 

QGroundControl can be downloaded and installed directly from the docs site: [here](https://docs.qgroundcontrol.com/master/en/getting_started/download_and_install.html)

### 5.2. Connect Pixhawk to PC

The Pixhawk interfaces with the PC through two methods: USB and/or wireless telemtry. For firmware updates, it is best to connect directly via USB. We will set up wireless telemtry in the coming weeks. 

With QGroundControl open on the PC, connect the Pixhawk using a micro USB cable. The ground control software will update and download the parameters from the flight controller. Very that the flight controller is connected by moving it around and observing movement in the heads-up display. (*[Figure 3](#fig3)*)

*<a name="fig3">![lab12_connectPixhawk](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab12_connectPixhawk.gif) <br>Figure 3: QGroundControl will download parameters from Pixhawk once connected.</a>*

### 5.3. Update Firmware and Operating System

The firmware update process should be completed in one pass to avoid any upgrade issues (such as bricking - if the firmware does not properly load). **First** review, and then perform the following steps to upgrade the firmware and operating system (*[Figure 4](#fig4)*):

1. Be sure that the Pixhawk is connected to QGroundControl, then click the *Q* icon in the upper left corner of the windows to open the settings pannel. 
2. Select *Vehicle Setup*
3. Choose the *Firmware* tab
4. Check the *Advanced* box to open the option to load the [ChibiOS](https://www.chibios.org/dokuwiki/doku.php) bootloader.
5. Click *Flash ChibiOS Bootloader* and verify message "Bootloader flash succeeded." Click *Ok*
6. Follow the instructions exactly as shown on the screen to perform the upgrade process. Including disconnecting and reconnecting the USB.
7. In the *Firmware Setup* window, select the following flightstack options (*[Figure 5](#fig5)*):
   - Operating System:        ChibiOS
   - Platform:             Multi-Rotor
   - Controller:            CubeBlack - 4.1.5
   - *Advanced settings*
   - Firmware Version:        Standard Version (stable)
8. Click *Ok*
9. Once firmware is loaded, the system will print *Upgrade complete*. Take a screenshot of this.

*<a name="fig4">![lab12_upgradeFirmware](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab12_upgradeFirmware.gif) <br>Figure 4: Process to upgrade Pixhawk firmware.</a>*

*<a name="fig5">![lab12_flightstackOptions](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab12_flightstackOptions.png) <br>Figure 5: Firmware upgrade flight stack options.</a>*

### 7. Turn in

On Brightspace, turning a link to your folder/directory containing the following files for this lab:

- 4.2. Exported CAD file
- 4.5. Printer Settings Text
- 4.5. Cura Project (saved as \*.3mf file type)
- 5.3. Firmware upgrade screenshot

---

:wave: Hey there! Notice anything you would like improved? Suggestions are always welcomed! Please [create a new issue](https://gitlab.com/purdue-uas/at21900/course-material/-/issues) on GitLab or [email](mailto:incoming+purdue-uas-at21900-course-material-32681911-47fahqsg66ijh2ikfun4yji7d-issue@incoming.gitlab.com?subject=[lab11]) your suggestion. 

[![CC BYNCSA 40](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
