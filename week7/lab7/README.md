# Lab 7 - Assemble and Test UAS Motors (17.5 points)

[TOC]

---

## 1. Objectives

The objective of this lab is to complete the next phase of building as UAS - adding the motor portion of the powertrain. Additionally, in this lab we will explore what it takes to make a motor move as we use simple buttons to replace the complexities of the electronic speed controller. We will accomplish this by creating a circuit capable of driving the motor by completing specific circuit routes within the motor design. 

We will also analyze and practice the equations governing brushless motors.

Finally, we will continue to develop the documentation that will accompany the finished UAS product.

---

## 2. Overview

Over the course of three weeks, we have discussed the operating principles behind the brushless DC motor (BLDC) in both lecture and in lab through disassembling and identifying the parts of a BLDC motor. Below is a refresher on the components and operating principles:

### 2.1 Rotor and Stator:

A BLDC motor is made of two main assemblies: the stationary component called the stator and a rotating component called the rotor. The stator contains several distinct coils of wire that carry electrical current. In *[Figure 1](#fig1)*, we can we see that the stator (right) has 12 such coils. The rotor (left) is composed of permanent magnets outlining the casing. In *[Figure 1](#fig1)*, we can see that there are 14 permanent magnets. This motor would be labelled as a 12N14P motor because there are 12 electromagnets in the stator and 14 permanent magnets in the rotor. The motor is driven by electromagnetic forces created between the wire coils in the stator and the permanent magnets of the rotor.

*<a name="fig1">![lab7_motorParts](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab7_motorParts.jpg) Figure 1: Brushless DC Motor pieces.</a>*

### 2.2 Electromagnets and Permanent Magnets:

When current passes through a coil of wire, it will induce a magnetic field that behaves much like a regular magnet with distinct north and south poles. *[Figure 2](#fig2)* shows the comparison between the electromagnet (stator) and the permanent magnet (rotor). When energized, each coil of the BLDC motor will become an electromagnet with poles that match the direction of the flow of current.

*<a name="fig2">![lab7_electromagnet](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab7_electromagnet.jpg) Figure 2: Comparison of magnet field between electromagnet (left) and permanent magnet (right)</a>*

To induce rotation of the BLDC, a current must be applied through the coils such that the poles of the electromagnet and permanent magnet either attract or repel depending on the direction of the current flow. *[Figure 3](#fig3)* shows the current waveform of each stator coil phase over a cycle. Consider this chart as you will be asked to draw the current flow thought the motor needed to reproduce this waveform.

*<a name="fig3">![lab7_currentWaveform](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab7_currentWaveform.jpg) Figure 3: Current waveform for three phase BLDC motor over three cycles.</a>*

The number of poles must be a factor of the number of phases. With the phase frequency constant, a change in the number of poles results in a inversely proportional reduction in speed, but increase in torque. This is much like changing the gears in a vehicle. Without changing out the engine in the car, we can get more torque (required for a hill climb or pulling) by changing the gear ration between the engine and the wheels. 

### 2.3 ESC

Recall from lecture that the ESC drives the motor by converting a signal from the flight controller into a high power output to the motor. The ESC automates the process of driving each of the three phases of the motor by completing the circuit that energizes the correct coils in the correct order. We will take a closer look at ESCs in Lab 8. For now, just understand that the ESC drives the motor by energizing the three phases in order. 

### 2.4 Motor Types and Sizes:

There are two types of BLDC motors: inrunners and outrunners. For inrunner motors, the rotor (with permanent magnets) rotates inside of the stator. For outrunner motors, however, the rotor rotates outside of the stator. See *[Figure 4](#fig4)* for the comparison between inrunner and outrunner motors. In this class, we will most commonly see outrunner motors as in *[Figure 1](#fig1)*. The operating principles between the two motors is identical.

*<a name="fig4">![lab7_inrunnerOutrunnerMotors](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab7_inrunnerOutrunnerMotors.jpg) Figure 4: Comparison between inrunner and outrunner motor. </a>[source](https://www.tytorobotics.com)*

## 3. Equipment List

This lab will require the following equipment. Each team is responsible for gathering the required equipment and tools.

- [ ] Quadcopter kit [HobbyKing X650F-V4](https://hobbyking.com/en_us/hobbyking-x650f-glass-fiber-quadcopter-frame-550mm.html)

- [ ] Toolkit

- [ ] Computer (one per group)

- [ ] Motor from Lab 3

- [ ] Clipped propeller

- [ ] Ardest electronics starter kit (2)
  
  - [ ] Breadboard
  
  - [ ] Buttons (6)
  
  - [ ] Wires (AR)

- [ ] Female bullet connectors (3)

- [ ] Soldering iron (and fixin's)

## 4. Assignment and Experiment

### 4.1 BLDC Motor Driver Circuit

In order to move a motor around, we need to incrementally apply power across the coils in three phases, each moving the motor ahead an amount determined by the number of stators and magnets. 

#### 4.1.1 Assignment: Pre-lab Questions (2.5 points)

1. Consider two motors with similar design, such as the motor in *[Figure 1](#fig1)*. However, one motor (motor A) has only six stators (and the appropriate number of permanent magnets) while the other motor (motor B) has twelve stators (and the appropriate number of permanent magnets). Answer the follow questions:
   1. > How many phases does each motor have?
      > 
      > Motor A:
      > 
      > Motor B:
   2. > Which motor will travel the greatest distance per energized phase?
   3. > Which motor would have the greater speed? Why?
   4. > Which motor would have the greater torque? Why?
   5. > Motors have pole counts in multiples of what number? Why?
   6. > Define KV:

#### 4.1.2 Motor Circuit Background (2.5 points)

Recall that a motor is driven by three phases of energizing coils in order. The coils are energized by completing a circuit through the motor. The motor can be represented as three resistor/inductor pairs that signify each of the three phases. (*[Figure 5](#fig5)*)

*<a name="fig5">![lab7_BLDCInternal](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab7_BLDCInternal.jpg) <br>Figure 5: Internal schematic representation of BLDC motor.</a>*

Each of the three wire terminals are connected to one of the wires in your physical motor (the red, yellow, black wires). 

Review *[Figure 3](#fig3)*. Consider that there are times in the cycles when one phase is high while another phase is low. That is, both are activated, but in different directions. 

> How is this possible?

> Given a power source (LiPo battery) draw a complete circuit for one phase. Annotate the relative direction that the current is flowing through the circuit. Do not over complicate this. Show your schematic to the instructor.

You see that the motor requires two wires to have power flowing at once in order to complete the circuit. We accomplish this typically by using an ESC. We know that the ESC controls the motor through [MOSFETs](https://en.wikipedia.org/wiki/MOSFET) which are essentially switches. Review *[Figure 6](#fig6)* below.

*<a name="fig6">![lab7_escSchematic](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab7_escSchematic.png) Figure 6: ESC schematic showing the individual MOSFETs that drive the motor.</a>* [source](https://electronoobs.com/eng_circuitos_tut19_2.php)

> How many MOSFETs (switches) are in an ESC? Why?

Now, we will build our own *simplified* ESC to move a motor.

#### 4.1.3 Construct Motor Circuit (4 points)

In place of MOSFETs, we will use buttons as they complete the same task (switching power on and off). We will need six buttons (the same as the number of MOSFETs in an ESC). Three buttons will control the connection to positive voltage and three buttons will control the connection to ground, respectively, in order to complete a circuit and energize one phase of coils. 

*<a name="fig7">![lab7_motorSchematic](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab7_motorSchematic.svg) Figure 7: Motor driver circuit using buttons.</a>*

Complete the following steps to build the circuit in *[Figure 7](#fig7)*

1. Using a [multimeter in resistance or continuity mode](https://www.fluke.com/en-us/learn/blog/digital-multimeters/how-to-measure-resistance), determine how the internal connection of the button is made. That is, when the button is pressed, which two leads become connected. This will be easiest to complete with two people. 

2. > Draw a schematic for the button; show the instructor.

3. Install the six buttons into the breadboard such that the two open leads connect to different terminal strips. See [SparkFun - Anatomy of a Breadboard](https://learn.sparkfun.com/tutorials/how-to-use-a-breadboard#anatomy-of-a-breadboard) to learn how the breadboard is connected internally.

4. Connect one side of the first three buttons to the (+) power rail. Connect one side of the remaining three buttons to the (-) power rail. Again, review [SparkFun - Anatomy of a Breadboard](https://learn.sparkfun.com/tutorials/how-to-use-a-breadboard#anatomy-of-a-breadboard) to learn how the breadboard is connected internally.

5. Solder three jumper wires individually into three female bullet connectors. Connect the bullet connectors to the motors and connect the motor yellow, black, and red wires, respectively, to separate terminal strips on the board (these should be separate from any other connections).

6. Connect buttons 1-3 to the yellow, black, and red motor wires, respectively, via the terminal strip.

7. Connect buttons 4-6 to the red, yellow, and black motor wires, respectively, via the terminal strip.

> Why are the connections in steps 5 and 6 offset?

7. Attach a clipped propeller to the motor.

8. When your wiring is complete, alert the instructor whom will verify the connections and instruct you to connect your motor control circuit to the power supply. We avoid using a battery in case of short circuit and in order to limit current.

9. Move your motor using the following sequence: (1,4), (2,5), (3,6), repeat.

10. Reverse the direction of rotation of your motor.

#### 4.1.4 Assignment: Post-lab Questions (2.5 points)

Answer the following questions:

1. > How many phases does it take to rotate the motor 360°?
2. > If the voltage is increased, how does this change the motor rotation?
3. > As a team, explain *[Figure 8](#fig8)*, below, to the instructor. 

*<a name="fig8">![lab7_statorCurrentFlow](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab7_statorCurrentFlow.jpeg) <br> Figure 8: Explain what is happening in this diagram. [source](https://howtomechatronics.com/how-it-works/how-brushless-motor-and-esc-work/)</a>*

### 4.2 Assemble UAS Motors and ESCs (6 points)

#### 4.2.1 Solder Bullet Connectors

1. Solder the three motor wires into the provided male bullet connectors. Refer to [Lab 1](https://gitlab.com/purdue-uas/at21900/course-material/-/blob/main/week1/lab1/README.md) for information on soldering bullet connectors.

2. Solder the three ESC wires into the provided female bullet connectors. Refer to [Lab 1](https://gitlab.com/purdue-uas/at21900/course-material/-/blob/main/week1/lab1/README.md) for information on soldering bullet connectors.

#### 4.2.2 Solder ESCs to Distribution Board

The ESCs are all powered from the battery through a central power distribution board. Complete the following steps and have your work inspected and **signed off in your maintenance document** by your team and the instructor.

1. Solder the ESC power-in wires to the appropriate location on the distribution board. There should be one ESC per side of the board. Be sure to match the polarity.

2. Solder the battery connector wires to the distribution board:
   
   1. Carefully strip the provided wires ensuring that no wire strands are cut.
   
   2. Twist and tin the wires.
   
   3. Insert the wires through the corresponding hole in the distribution board and solder. 

3. Solder an XT-60 connector to the other end of the wires using the provided heat shrink to cover the solder cups.

#### 4.2.3 Attach Motor to UAS Frame

1. Assemble motors to aluminum motor mounts using the provided flat head screws.

2. Attach the motor assemblies to the fiberglass motor mounts on the frame. The frame is slotted to allow for different sized motors to be mounted. 

### 4.3 Complete Assembly Documentation

1. In not already completed in Lab 6, add the motor subassembly to your BOM.

2. Add the work completed today to your maintenance log, including the work performed, the person performing the work, and an inspector's sign off. 

## 5 Turn It In:

1. Submit the questions in this document either to GitLab or in a document to Brightspace.

2. Documentation should already be shared with instructor from Lab 6.

---

:wave: Hey there! Notice anything you would like improved? Suggestions are always welcomed! Please [create a new issue](https://gitlab.com/purdue-uas/at21900/course-material/-/issues) on GitLab or [email](mailto:incoming+purdue-uas-at21900-course-material-32681911-47fahqsg66ijh2ikfun4yji7d-issue@incoming.gitlab.com?subject=[lab3]) your suggestion. 

[![CC BYNCSA 40](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
