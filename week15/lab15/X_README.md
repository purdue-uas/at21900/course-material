# Lab 14 - Wire Harness and Calibrations  (30 points)

[TOC]

---

## 1. Objectives

The objective of this lab is to complete the physical UAS build and the flight controller calibrations. Upon completion of this lab, the UAS should be fully functional and ready to fly.

---

## 2. Overview

### 2.1. Wire Harness

Building a proper wire hardness is critical to safe aircraft operations. In the 1980s, because of numerous wiring-related accidents, the FAA set forth regulations to improve wiring technique to prevent common issues in design, manufacturing and repair. There are several factors to consider in making a safe wire harness:

1. Routing/Chafing - wires are subject to vibration-induces chafing of the insulation material. That is, wires should be routed to avoid sharp edges that can degrade insulation integrity. (*[Figure 1](#fig1)*)

*<a name="fig1">![lab14_routing](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_routing.png) <br>Figure 1: Examples of routing and chafing example and results. [[Source](https://www.faa.gov/training_testing/training/air_training_program/job_aids/media/ewis_job-aid_2.0_printable.pdf)]</a>*

2. Coil and stow - wires that are not in use on the aircraft must be stowed in a manner to prevent short circuits and arcing. Unused wires can occur during repairs and manufacturing. These wires must be properly terminated. (*[Figure 2](#fig2)*)

*<a name="fig2">![lab14_coilAndStow](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_coilAndStow.png) <br>Figure 2: Examples of improper wire termination and resulting arcing. [[Source](https://www.faa.gov/training_testing/training/air_training_program/job_aids/media/ewis_job-aid_2.0_printable.pdf)]</a>*

3. Bend radius - The accepted standard (AC 43.13-1b) for wire bend radius is 10 times the outer diameter of the largest diameter wire in the harness. So, a 5 mm wire requires a 50 mm bend radius. This prevent damaging the internal wire structure. Additionally, it is important to ensure that there is no strain on the wires. (*[Figure 3](#fig3)*)

*<a name="fig3">![lab14_routing](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_routing.png) <br>Figure 3: Examples of improper wire bend radius. The radius should be 10 times the diameter of the largest wire. [[Source](https://www.faa.gov/training_testing/training/air_training_program/job_aids/media/ewis_job-aid_2.0_printable.pdf)]</a>*

Additionally, there are several techniques to routing wires that allow for easy problem tracing and repair. By managing wires properly from the beginning, electrical issues can be caught and rectified quicker resulting in less aircraft downtime. The following are a few harness techniques that can be used depending on the application. 

1. "Y" type breakout - this breakout style is useful for diverging from the central wire harness when the harness needs to branch to another location in the aircraft. The branch should be carefully separated using plastic zip-ties. (*[Figure 4](#fig4)*)

*<a name="fig4">![lab14_YBundle](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_YBundle.png) <br>Figure 4: Examples of "Y" type wire bundle. [[Source](https://www.faa.gov/training_testing/training/air_training_program/job_aids/media/ewis_job-aid_2.0_printable.pdf)]</a>*

2. "T" type breakout - this breakout style is used when portions of wire hardnesses from both directions depart to a different direction. (*[Figure 5](#fig5)*)

*<a name="fig5">![lab14_TBundle](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_TBundle.png) <br>Figure 5: Examples of "T" type wire bundle. [[Source](https://www.faa.gov/training_testing/training/air_training_program/job_aids/media/ewis_job-aid_2.0_printable.pdf)]</a>*

3. Complex type breakout - this breakout style is useful when certain wires route from the central harness to different location on a terminal block (or similar). (*[Figure 6](#fig6)*)

*<a name="fig6">![lab14_complexBundle](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_complexBundle.png) <br>Figure 6: Examples of complex wire bundle. [[Source](https://www.faa.gov/training_testing/training/air_training_program/job_aids/media/ewis_job-aid_2.0_printable.pdf)]</a>*

### 2.2. System Calibration

As discussed in [lecture 13.2](https://gitlab.com/purdue-uas/at21900/course-material/-/blob/main/week13/lecture13.2/presentation13.2.pptx), the Pixhawk flight controller is capable of providing flight control to a wide array of [UAS designs](https://ardupilot.org/copter/docs/common-all-vehicle-types.html#common-all-vehicle-types). Because of this flexibility, it is mandatory to configure and calibrate the flight controller sensors with the correct parameters for stable flight. This lab will calibrate the accelerometers, compasses, RC transmitter, ESCs, motor range, failsafes, and flight modes.

## 3. Connection Verification

Verify from the table below that all connections on the UAS have been made properly. 

| From         | Signal                         | To                   |
| ------------ | ------------------------------ | -------------------- |
| ESC 1-4      | 5V, GND, Signal                | Pixhawk Main Out 1-4 |
| GPS          | Serial Communication           | Pixhawk GPS 1        |
| Buzzer       | Buzzer Voltage                 | Pixhawk USB          |
| Power Module | Power Management Communication | Pixhawk Power 1      |
| RC Receiver  | RC Signal                      | Pixhawk RC IN        |

## 4. Wire Harness (10 points)

Review [section 2.1.](#21-wire-harness) above. As your wiring comes together, use zip ties to create clean and neat wire harnesses to manage the numerous wires to/from the flight controller. Your UAS wiring will be graded on the criteria in [section 2.1.](#21-wire-harness) and on appearance. 

## 5. Setup and Calibrate Flight Controller (20 points)

In this section we will calibrate all of the components of the UAS. This allows us to use unrelated components by mapping their inputs and outputs to the ArduPilot parameters. 

### 5.1. Setup Frame Type

Review the [Frame Class and Type Configuration ArduPilot Docs](https://ardupilot.org/copter/docs/frame-type-configuration.html)

Set up the aircraft as a "Quad X" frame vehicle. This should correlate appropriately with the orientation of the Pixhawk. (*[Figure 7](#fig7)*)

*<a name="fig7">![lab14_frameType](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_frameType.gif) <br>Figure 7: Setup frame type as "Quad X."</a>*

### 5.2. Setup RC Transmitter

Answer the following questions:

1. How many channels does the transmitter have? `    `
2. How many channels does the receiver have? `    `

You will notice that they do not match. The transmitter has four channels on the control sticks and there is room for two auxiliary channels. At the moment, the auxiliary channels are on rotary switches, which is not useful for discrete settings (like changing moves, etc.)

Following [this video](https://www.youtube.com/embed/4Ix2YS-pMJ8), set the SWC switch to channel 5 and the SWD switch to channel 6. We will use channel 5 to control the flight mode, and channel 6 to control the return to launch feature. 

### 5.3. Calibrate RC Input

Review the [Radio Control Calibration ArduPilot Docs](https://ardupilot.org/copter/docs/common-radio-control-calibration.html)

When the aircraft is powered by the computer USB, the RC receiver should blink a red LED. If not, check your wiring (the signal wire is on the notch side on the iBus/Servo diagram). Power on the transmitter and verify that the LED changes to solid red indicating that the TX/RX are bound. 

Calibrate the RC according to *[Figure 8](#fig8)* below. Be sure to follow the calibration instructions exactly including zeroing the trim settings. 

*<a name="fig8">![lab14_rcCalibration](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_rcCalibration.gif) <br>Figure 8: RC Calibration.</a>*

### 5.4. Calibrate Accelerometer

Review the [Accelerometer Calibration ArduPilot Docs](https://ardupilot.org/copter/docs/common-accelerometer-calibration.html).

Perform the accelerometer calibration according to *([Figure 9](#fig9))* below. Be sure that the drone remains still while the accelerometer values are being captured. Additionally, follow the proper orientation pattern. *[Figure 10](#fig10)*

*<a name="fig9">![lab14_accelCalibration](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_accelCalibration.gif) <br>Figure 9: Accelerometer Calibration.</a>*

*<a name="fig10">![lab14_accelOrientation](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_accelOrientation.jpg) <br>Figure 10: Accelerometer calibration drone orientation. [[Source](https://ardupilot.org/copter/docs/common-accelerometer-calibration.html)]</a>*

### 5.5. Calibrate Compass

Review the [Compass Calibration ArduPilot Docs](https://ardupilot.org/copter/docs/common-compass-calibration-in-mission-planner.html).

Perform the compass calibration ("copter dance") according to *[Figure 11](#fig11)* below.

*<a name="fig11">![lab14_compassCalibration](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_compassCalibration.gif) <br>Figure 11: Compass Calibration.</a>*

### 5.6. Set Flight Modes

Now that we have configured and calibrated the RC transmitter/receiver, we can set flight modes based on the auxiliary channels from the transmitter.

Review the [RC Transmitter Flight Mode Configuration ArduPilot Docs](https://ardupilot.org/copter/docs/common-rc-transmitter-flight-mode-configuration.html).

In this course we will start with three flight modes - Stabilize, Alt Hold, and Loiter. After a few successful flights, we will add Auto mode. Additionally, on channel 6, we will add a RTL signal to return the drone to the launch point (like auto land on DJI). Review how the three modes work by reading the [Flight Modes ArduPilot Docs](https://ardupilot.org/copter/docs/flight-modes.html). It is very important to understand what to expect from the aircraft before flight. 

Set the flight modes according to *[Figure 12](#fig12)* below.

*<a name="fig12">![lab14_flightMode](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_flightMode.gif) <br>Figure 12: Configure flight modes.</a>*

### 5.7. Calibrate ESCs

Just as we calibrated/mapped the signals from the RC receiver, we must also map the ESCs to the signal from the flight controller. To calibrate the input signal into the ESCs, follow the steps below:

1. Remove all props from the drone.
2. Turn all switches on transmitter to neutral (usually up).
3. Set throttle to max and power on the transmitter. 
4. Connect a LiPo to power the drone. The flight controller will signal with multi-colored lights that it is ready to enter into ESC calibration mode. 
5. Unplug the battery, wait a few seconds, the reconnect the battery.
6. Push and hold the safety/arm switch until it is solid red.
7. Listen for a series of tones from the ESCs signaling the max throttle has been set.
8. Move throttle to min.
9. The ESCs will emit another series of tones signaling that min throttle has been set. 
10. The ESCs should now be live. 
11. Test that the motors spin up when the throttle is pushed up.

### 5.8. Test Motor Spin

Recall that two of the motors must spin counterclockwise and two must spin clockwise. With the propellers removed and the battery plugged in, individually check the spin orientation of each motor on at a time using the sliders in the Motors tab in Vehicle Setup in QGroundControl. *([Figure 13](#fig13))* Verify that the motor rotation matches *[figure 14](#fig14)* below.

*<a name="fig13">![lab14_motorTest](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_motorTest.png) <br>Figure 13: QGroundControl motor control sliders.</a>*

*<a name="fig14">![lab14_motorOrder](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_motorOrder.png) <br>Figure 14: Motor order. [[Source](https://ardupilot.org/copter/docs/connect-escs-and-motors.html)]</a>*

### 5.9. Set Failsafes

Failsafes are part of any good crew resource management. Failsafes should be set and predetermined before flight as well as verified with the team so that everyone knows what to expect when an event occurs. Set and verify the following failsafes:

#### 5.9.1. Radio Failsafe

Review [when the failsafe will trigger](https://ardupilot.org/copter/docs/radio-failsafe.html#when-the-failsafe-will-trigger) as well as [what will happen](https://ardupilot.org/copter/docs/radio-failsafe.html#what-will-happen) when the failsafe is triggered.

First we must tell the receiver what signal to send when it loses connection with the transmitter. To set this, follow [this video (from 0:00 - 3:40)](https://www.youtube.com/embed/4DGZOaQOzJU) to set up the failsafe on channel 3 (throttle) as well as test the fail safe (this can be done in QGroundControl in the "Radio" tab. When power is cut on the transmitter, throttle should return to zero).

When the receiver loses connection at the moment, the throttle will be cut to zero. This is great for preventing flyaways, but will result in a power off landing... Some call this a crash. 

Instead, we will have the flight controller perform a RTL to safely land so that we can fix the issue. Follow the steps in *[Figure 15](#fig15)* below to verify that a zero throttle value will trigger RTL. 

*<a name="fig15">![lab14_throttleFailsafe](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_throttleFailsafe.gif) <br>Figure 15: Verify throttle failsafe is set to RTL.</a>*

Check that the setting updated the failsafe parameters, by search for "FS_THR" in the parameters list. Show the instructor your values. (*[Figure 16](#fig16)*)

*<a name="fig16">![lab14_throttleFailsafeParameter](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_throttleFailsafeParameter.png) <br>Figure 16: Verify throttle failsafe setting in parameters.</a>*

#### 5.9.2. Setup Power Module

In order to use the battery failsafe feature, we must first configure ArduPilot for the connected power module. In QGroundControl, navigate to the Power tab in the vehicle parameters. (*[Figure 17](#fig17)*)

*<a name="fig17">![lab14_powerModuleSetup](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_powerModuleSetup.jpg) <br>Figure 17: Battery module configuration.</a>*

Input the proper values for the battery we are using. Then run the [QGroundControl calibration](https://docs.qgroundcontrol.com/master/en/SetupView/Power.html#battery-voltagecurrent-calibration).

#### 5.9.3. Battery Failsafe

Review [when the failsafe will trigger](https://ardupilot.org/copter/docs/failsafe-battery.html#when-the-failsafe-will-trigger) as well as [what will happen](https://ardupilot.org/copter/docs/failsafe-battery.html#what-will-happen) when the failsafe is triggered.

The power module measures the battery voltage in realtime. This information can be used to determine the remaining capacity of the battery. To ensure that we land before the battery is depleted, set the UAS to return to launch when the battery reaches critical level.

Access the battery failsafe feature in QGroundControl in the Vehicle Setup > Safety tab. (*[Figure 18](#fig18)*)

*<a name="fig18">![lab14_batteryFailsafe](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_batteryFailsafe.jpg) <br>Figure 18: Battery failsafe parameters.</a>*

Determine the best settings for your team. I suggest the following, but this can vary based on the mission.

| Setting                    | Value                        |
| -------------------------- | ---------------------------- |
| Low Action                 | RTL                          |
| Critical Action            | Land                         |
| Low Voltage Threshold      | `What should this value be?` |
| Critical voltage threshold | 0.0                          |
| Low mAh threshold          | 0                            |
| Critical mAh threshold     | 0                            |

#### 5.9.4. Ground Control Station Failsafe

This failsafe is important for UAS that are controlled directly from QGroundControl (or similar). In our case, we will not be controlling the UAS from QGroundControl, but instead directly with the RC transmitter. However, when we connect wireless radio telemetry modules to the UAS, this failsafe will be important. 

For now, this failsafe is optional, but can be setup now. First, review [when the failsafe will trigger](https://ardupilot.org/copter/docs/gcs-failsafe.html#when-the-failsafe-will-trigger) and [what will happen](https://ardupilot.org/copter/docs/gcs-failsafe.html#what-will-happen). Then set the ground control failsafe preferably to "Enabled always RTL." (*[Figure 19](#fig19)*)

*<a name="fig19">![lab14_gcsFailsafe](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_gcsFailsafe.jpg) <br>Figure 19: Ground control station failsafe.</a>*

#### 5.9.5. EKF Failsafe

The EKF failsafe triggers when there is a large variance in the compass. First, review [when the failsafe will trigger](https://ardupilot.org/copter/docs/ekf-inav-failsafe.html#when-will-it-trigger) and [what will happen](https://ardupilot.org/copter/docs/ekf-inav-failsafe.html#what-will-happen-when-the-failsafe-triggers). Then set the EKF failsafe under the Safety tab in Vehicle Setup to "warn only." (*[Figure 20](#fig20)*)

*<a name="fig20">![lab14_ekfFailsafe](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_ekfFailsafe.jpg) <br>Figure 20: EKF failsafe.</a>*

#### 5.9.6. Vibration Failsafe

Ahhhh... [Good Vibrations](https://www.youtube.com/watch?v=Eab_beh07HU). Although we have preemptively mitigated extraneous vibrations through vibration damping mounts, there may be certain condition that induce excessive vibration into the system (wind events, payload, etc.). First, review [when the failsafe will trigger](https://ardupilot.org/copter/docs/vibration-failsafe.html#when-the-failsafe-will-trigger) and [what will happen](https://ardupilot.org/copter/docs/vibration-failsafe.html#what-will-happen). 

Because this failsafe is enabled by default, the switch to turn it on/of is not in the safety setup wizard. So, we have to set/verify this in the full parameters list. (*[Figure 21](#fig21)*) Navigate to the Parameters tab in the Vehicle Setup. Search for [FS_VIBE_ENABLE](https://ardupilot.org/copter/docs/parameters.html#fs-vibe-enable). The value should be set to 1 or "enabled". If is is 0 or "disabled," change it in the parameter editor and save.

*<a name="fig21">![lab14_vibrationFailsafe](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab14_vibrationFailsafe.jpg) <br>Figure 21: Search parameter list for FS_VIBE_ENABLE.</a>*

#### 5.9.7. Arming Checks

By default the vehicle should check all onboard and external sensors before allowing the motors to arm. Verify that "All" is checked under Arming Checks in the Safety tab in Vehicle Setup.

The arming checks provide very useful data to the HUD in QGroundControl. Interpret what the errors mean by reviewing the potential [failure messages](https://ardupilot.org/copter/docs/common-prearm-safety-checks.html#failure-messages)

### 5.10. First Flight

The following steps assume the following configuration:

1. Transmitter on.
2. Propellers removed. (reinstall for flight)
3. LiPo battery connected to UAS.

#### 5.10.1. Arming/Disarming Motors

The ArduPilot/Pixhawk Cube uses a two-part arming system - A physical safety switch and a RC input command (much like DJI). The physical safety switch is located on top of the GPS unit. 

First, press and hold the physical arm switch for five seconds. Then hold throttle low and yaw right for five seconds (but no more than 15 - this enables autotune). The ESCs should chime, the GPS LEDs should turn solid, and the motors should spin up. 

To disarm, hold throttle low and yaw left for five seconds. 

#### 5.10.2. Test Flight

If all systems seem to be operating nominally, reinstall the propellers. Set the drone to stabilize flight mode, arm the system, and take off. The first flight should be *low and slow* and should more hover more than a few inches off the ground. 

### 5.11. Tuning

Tuning is beyond the scope of this course, but students are encouraged to experiment with tuning settings. Tuning is one of the most challenging and time consuming parts of making a commercially viable drone. Those interested in testing tuning beyond defaults should review the [ArduPilot Tuning Doc](https://ardupilot.org/copter/docs/common-tuning.html)

## 6. Backup Parameters

Now that you have configured your UAS, it may look the same as the other teams, but it has parameter values unique to your groups calibration work. Backup the parameters to your team folder/repository with this lab. Parameters can be backed up from the Parameters tab in Vehicle Setup in QGroundControl by clicking on the [Tools](https://docs.qgroundcontrol.com/master/en/SetupView/Parameters.html#tools) button and selecting "Save to file..."

## 7. Documentation Update

Be sure to document all installation steps, parameter settings, and first flight analysis. Continue to document operational procedures for your UAS. Refer to [Lab 6](https://gitlab.com/purdue-uas/at21900/course-material/-/blob/main/week6/lab6/README.md) for documentation information.

## 10.Turn in

Upload completed lab questions, parameters, and updated documentation to your team folder/repository. 

---

:wave: Hey there! Notice anything you would like improved? Suggestions are always welcomed! Please [create a new issue](https://gitlab.com/purdue-uas/at21900/course-material/-/issues) on GitLab or [email](mailto:incoming+purdue-uas-at21900-course-material-32681911-47fahqsg66ijh2ikfun4yji7d-issue@incoming.gitlab.com?subject=[lab11]) your suggestion. 

[![CC BYNCSA 40](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
