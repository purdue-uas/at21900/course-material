# Lab 0 - Lab Safety and Soldering, and GitLab

[[_TOC_]]

## Introduction

This lab will focus on safety, soldering, and GitLab. Safety in the AT 21900 lab is of the utmost importance - any unsafe behaviors will result in grade reduction. Soldering and GitLab are two skills that will distinguish you as a UAS designer rather than a UAS user.

Throughout this lab, any safety consideration considerations will be called out in the following notation:

> :warning: **Burn Warning:** Soldering iron can reach temperatures between **600°- 650°F (316°- 343°C)** and can case severe burns. 

## Safety

Safety makes the difference between drones as a profession and drones as a toy. The instructor will give a brief, but very important tour of the lab space to note safety devices including, but not limited to: eye wash stations, emergency showers, flamable cabinets, sink, etc. Additionally, the instructor will discuss safety protocols for LiPo batteries, lead solder, power tools, and electricity.

## Soldering

Soldering is a crucial skill for UAS developers. From prototyping to final design, UAS developers use soldering to create reliable, clean connections for power, signals, and data as well as for structural integrity. This lab will cover the basics of soldering technique required to solder lap joints, barrel connectors, and XT-60 connectors.

### Parts List

**Required**

- [ ] Mask

- [ ] Safety Glasses

- [ ] (1) Soldering Station (Weller WLC100)
  <img title="" src="https://www.tubesandmore.com/sites/default/files/uc_products/s-twlc100_copy.png" alt="soldering station" width="241">[](https://www.tubesandmore.com/products/solder-station-weller-wlc100)

> :warning: **Burn Warning:** Soldering iron can reach temperatures between **600°- 650°F (316°- 343°C)** and can case severe burns.

- [ ] (1) Spool Solder
  <img title="" src="https://core-electronics.com.au/media/catalog/product/cache/1/image/650x650/fe1bcd18654db18f328c2faaaf3c690a/0/0.8mm_200gm_roll_60_40_leaded_solder__t1100__00.jpg" alt="solder" width="202">[](https://core-electronics.com.au/0-8mm-200gm-roll-60-40-leaded-solder.html)

> :warning: **Lead Warning:** Solder contains lead. Proper precautions should be taken when handling lead including washing hands before eating or drinking.

- [ ] (1) Heat gun (HG-300D CUL)
  <img title="" src="https://www.yeuchyuan.com.tw/comm/upimage/p_181119_08305.jpg" alt="heatgun" width="208">[](https://www.yeuchyuan.com.tw/comm/upimage/p_181119_08305.jpg)

> :warning: **Burn Warning:** Heat guns can reach temperatures over **750°F (499°C)** and can cause severe burns.

- [ ] (1) Helping Hands
  <img title="" src="http://wonderfulengineering.com/wp-content/uploads/2015/10/Best-soldering-helping-hands-fingers-8.jpg" alt="helpinghands" width="212">[](http://wonderfulengineering.com/wp-content/uploads/2015/10/Best-soldering-helping-hands-fingers-8.jpg)

- [ ] (1) Electronic Speed Controller (ESC)
  <img title="" src="https://www.robotshop.com/media/catalog/product/cache/image/1350x/9df78eab33525d08d6e5fb8d27136e95/3/5/35a-bec-multirotor-esc-without-connectors.jpg" alt="esc" width="214">[](https://www.robotshop.com/media/catalog/product/cache/image/1350x/9df78eab33525d08d6e5fb8d27136e95/3/5/35a-bec-multirotor-esc-without-connectors.jpg)

- [ ] (1) Female XT-60 Connector
  <img title="" src="https://robu.in/wp-content/uploads/2014/12/robu-1-2.jpg" alt="xt60" width="214">[](https://robu.in/wp-content/uploads/2014/12/robu-1-2.jpg)

- [ ] (3) Female Bullet Connectors
  <img src="https://www.valuehobby.com/media/catalog/product/cache/1/image/600x600/34dac341956dbc2da4a692159cfaef5d/1/5/1575-3.jpg" title="" alt="bulletconnector" width="214"> [](https://www.valuehobby.com/media/catalog/product/cache/1/image/600x600/34dac341956dbc2da4a692159cfaef5d/1/5/1575-3.jpg)

- [ ] (5) Pieces Heat Shrink
  <img src="https://www.gammaelectronics.net/wp-content/uploads/2021/05/Cut-heat-shrink-tabletop-for-web-v1.jpg" title="" alt="heatshrink" width="214">[](https://www.gammaelectronics.net/wp-content/uploads/2021/05/Cut-heat-shrink-tabletop-for-web-v1.jpg)

- [ ] (1) Servo Cable
  <img src="https://imgaz1.staticbg.com/thumb/large/upload/2012/chenjianwei/SKU085661.1.JPG.webp" title="" alt="servocable" width="206">[](https://imgaz1.staticbg.com/thumb/large/upload/2012/chenjianwei/SKU085661.1.JPG.webp)

**Optional**

- [ ] Solder Wick
  <img src="https://i5.walmartimages.com/asr/f5de40b3-5035-40af-a00d-8012cacf9ff1_1.5cf77cc8503298a6e397d6443b392762.jpeg?odnHeight=612&odnWidth=612&odnBg=FFFFFF" title="" alt="solderwick" width="209">[](https://i5.walmartimages.com/asr/f5de40b3-5035-40af-a00d-8012cacf9ff1_1.5cf77cc8503298a6e397d6443b392762.jpeg?odnHeight=612&odnWidth=612&odnBg=FFFFFF)

- [ ] Solder Sucker
  <img title="" src="https://www.simbatools.co.ke/wp-content/uploads/2017/02/Metal-Solder-Sucker-185-mm.jpg" alt="soldersucker" width="205">[](https://www.simbatools.co.ke/wp-content/uploads/2017/02/Metal-Solder-Sucker-185-mm.jpg)

- [ ] Wire Stripper
  <img src="https://www.electriduct.com/assets/images/8-22awg-automatic-wire-stripper.jpg" title="" alt="wirestripper" width="201">[](https://www.electriduct.com/assets/images/8-22awg-automatic-wire-stripper.jpg)

### Instructions

#### Setting up your station:

1. Dampen the provided sponge and reinsert it into the base station.

2. With the power switches off, plug in the head gun and the soldering iron in a way that prevents the wires from interfering with your work.

3. Organize your parts and clear your working area.

#### Soldering a lap join:

A lap joint is a very common, simple soldering technique to connect two or more wires. The lap joint provides great signal connection as well as structural durability if done correctly.

**The Goal:** ![lapjoint](https://cdn.sparkfun.com/r/600-600/assets/learn_tutorials/4/1/Splicing_Wire-Solder_Wire_Ends_Thoroughly.jpg)[](https://cdn.sparkfun.com/r/600-600/assets/learn_tutorials/4/1/Splicing_Wire-Solder_Wire_Ends_Thoroughly.jpg)

**Demonstration:** The instructor will demonstrate how to solder a lap joint. Additionally, follow [this guide at Spark Fun](https://learn.sparkfun.com/tutorials/working-with-wire/how-to-splice-wires) for instruction on soldering a lap joint. 

**Questions to Consider:**

1. How should the solder flow?

2. Explain the amount of solder that should be on the lap joint.

3. What does it mean to tin the wires?

4. What does tinning the wires do?

5. What did you notice happens to the wire insulation on this wire? Why?

#### Soldering an XT-60 Connector

The XT-60 connector is a very common power adapter used on UAS to connect the batter to the electronic speed controllers. This connection can be expected to carry a large amount of current, so the connection must be clean to avoid increased resistance.

**The Goal:**
![xt-60solder](https://www.rchelicopterfun.com/images/shiny-solder-job-500px.jpg)

**Demonstration:** The instructor will demonstrate how to solder a XT-60 connector. Additionally, follow [this YouTube tutorial](https://www.youtube.com/watch?v=91r5Qk-pKB8) for instruction on soldering the XT-90 connector (which is similar to the XT-60) and applying heat shrink.

**Questions to Consider**

1. Does the XT-60 connector have polarity?

2. What could happen if soldering an XT-60 connected that is connected to a LiPo battery?

3. Why is heat shrink important in this application?

4. Which side does the positive lead connect to?

#### Soldering a Bullet Connector

Bullet connectors are very stable connectors used in mission critical, high power connections like connecting the ESCs to the motors. 

**The Goal:** ![bulletsolder](https://i.ytimg.com/vi/fdTDTuBb3pQ/maxresdefault.jpg)

**Demonstration:** The instructor will demonstrate how to solder bullet connectors to the ESCs. Additionally, follow [this YouTube tutorial](https://www.youtube.com/watch?v=fdTDTuBb3pQ) for instruction on soldering bullet connectors and applying heat shrink.

**Questions to Consider**

1. What is flux?

2. What do you notice about the wire the longer you heat it (beside it getting warmer!)?

## GitLab

We will use [GitLab](https://gitlab.com) through out the entirety of this course, both for assignment as well as hosting project data, code, and the project wiki. Please [register](https://gitlab.com/users/sign_up) with the following specifications:

1. Use your Purdue email. Do not register with Google or GitHub SSO options.

2. So long as you use your Purdue email, you may set your account name to anything appropriate.

Optional (but encouraged):

> We will also make use of the [WorkingCopy iOS App](https://workingcopy.app/) for accessing GitLab on our iPads. This is usually a paid application, but as a student, you can access it for free using the following steps:
> 
> 1. Sign up for a [GitHub account](https://github.com/signup)
> 
> 2. Then, sign up for the [GitHub Student Developer Pack](https://education.github.com/pack).
> 
> 3. After you have been verified, follow the instructions [here](https://workingcopy.app/education/) to get access to WorkingCopy for free while you are a student.

## Assignments:

1. Complete lab questions on [Brightspace](https://purdue.brightspace.com) by the end of your assigned lab period.

[![CC BYNCSA 40](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
