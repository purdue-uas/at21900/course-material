# Lab 3 - Reverse Engineering a Multi-rotor UAS (50 pts.)

[TOC]

---

## 1. Objectives

The objective of this lab is to use the principles of reverse engineering to uncover the lower-level functions of a multi-rotor UAS. To do this you will document and measure the disassembly process of the individual UAS components to understand how they work together as a system.

We will develop the skill to break a system into it's individual components, analyze the parts, and describe how they work together.

---

## 2. Overview

Reverse engineering is taking apart a system or device to understand how it functions. This is done commonly in industry to learn how a competitor built a product with an intention to improve or simply reproduce it. In the UAS world, this is very common in commercial drones - with in a few months of a new DJI drone release, there will be a knock off that has been produced through the process of disassembling the OEM DJI to reproduce it for lower cost - a process that is understandably illegal in some countries. However, reverse engineering can play a big role in education. By studying the work of other professionals, we can gain an insight and understanding of the design process. However, this requires careful documentation and organization of the individual pieces.

The process of disassembly can also reveal potential vulnerabilities in both software, hardware, and design. As you disassemble your UAS, consider any vulnerabilities in the current system and think of ways that you could improve the design.

---

## 3. Equipment List

This lab will require the following equipment. Each team is responsible for gathering the required equipment and tools.

- [ ] [Toolkit](#tool-report-5-pts)

- [ ] Translucent Plastic Compartment Organizer or egg carton

- [ ] [Soldering Iron and Soldering Equipment](https://gitlab.com/purdue-uas/at21900/course-material/-/blob/main/week1/lab1/README.md) (communal item)

- [ ] Scale (communal item)

- [ ] Permanent Marker

- [ ] Propeller Balancer (communal item)
  
  - [ ] Tape
  
  - [ ] Sand Paper

- [ ] Drone

- [ ] Battery

- [ ] Printer Paper or iPad for sketches/diagrams.

## 4. Assignment

Each assignment should be completed in the team copy of this document **Directly below** it's corresponding heading. This lab document should be cloned from your team repository titled `lab3`. Your completed assignment should then be pushed back to the repository. The assignment will be pulled and graded on the due date - no late work.

> :green_book: Please read through the assignments completely before beginning disassembly as the assignments are not necessarily chronological.

### 4.1 Tool Report (5 pts.)

The Purdue UAS program is in the process of incorporating the [Japanese 5S organizational methodology](https://asq.org/quality-resources/lean/five-s-tutorial) into both the teaching and research space as we prepare students for jobs in industry that focus on [lean management practices](https://asq.org/quality-resources/lean). The table below  (copied from the [ASQ website](https://asq.org/quality-resources/lean/five-s-tutorial)) summarizes the 5S's:

| Japanese | Translated  | English      | Definition                                                                                                    |
| -------- | ----------- | ------------ | ------------------------------------------------------------------------------------------------------------- |
| Seiri    | organize    | sort         | Eliminate whatever is not needed by separating needed tools, parts, and instructions from unneeded materials. |
| Seiton   | orderliness | set in order | Organize whatever remains by neatly arranging and identifying parts and tools for ease of use.                |
| Seiso    | cleanliness | shine        | Clean the work area by conducting a cleanup campaign.                                                         |
| Seiketsu | standardize | standardize  | Schedule regular cleaning and maintenance by conducting seiri, seiton, and seiso daily.                       |
| Shitsuke | discipline  | sustain      | Make 5S a way of life by forming the habit of always following the first four S’s.                            |

As part of following 5S practices, this lab will require that each team be responsible for verifying their toolkits before and after each lab for missing tools, broken tools, or additional tools. Each team must take the time to complete the toolkit checklist available [here](https://gitlab.com/purdue-uas/at21900/course-material/-/blob/main/resources/toolkit-checklist.md) once when checking out a tool kit and again when checking it back in. Record of the toolkit ID should be included in each lab assignment. Failure to report missing or broken tools as well as failure to maintain tool organization will result is point reduction for the lab assignment.

#### Toolkit ID

By filling in your toolkit ID, you verify that your have received all of the tools listed in the [toolkit checklist](https://gitlab.com/purdue-uas/at21900/course-material/-/blob/main/resources/toolkit-checklist.md), and have completed the checklist at both checkout and checkin.

**Toolkit ID:** **`          `**

### 4.2 Team Report Abstract (10 pts.)

This lab will require a group report abstract to summarize what would otherwise be a full lab report. Guidelines for writing an informational abstract can be found on the [Purdue OWL](https://owl.purdue.edu/owl/subject_specific_writing/professional_technical_writing/technical_reports_and_report_abstracts/index.html) website. The team's abstract will be graded as follows:

| \#  | Description                                                                                                                                                                                                                                                                | Points |
|:---:|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |:------:|
| 1.  | **Purpose & scope**<br/>- What was the objective of the lab?<br/>- What did you accomplish?                                                                                                                                                                                | 1      |
| 2.  | **Results**<br/>- What were the results of the lab?<br/>- What did you find out?<br/>- What are the significance of your findings?                                                                                                                                         | 3      |
| 3.  | **Conclusions**<br/>- What conclusions did your team make?<br/>- What is the significance of what you learned?<br/>- How does this fit into the big picture of UAS design?<br/>- Recommendations for future UAS designs?<br/>     - Vulnerabilities in the current design? | 3      |
| 4.  | **Writing**<br/>- Does the abstract follow logical order?<br/>- Is it concise?<br/>- Is it intelligible to a wide audience?                                                                                                                                                | 3      |

### 4.3 Teardown Instructions (15 pts.)

In addition to an abstract, teams will be expected to create teardown instructions for their UAS. The optimal format for team down instructions involves the very intentional and frequent use of photos, diagrams, and tables. Correctly written teardown instructions also serve as build instructions when reversed - keep this in mind as you document disassembly. Some of the most thorough teardown instructions for common consumer electronics are written by [iFixit](https://www.ifixit.com) a [Right To Repair](https://www.repair.org) advocacy group. Your team's instructions should follow a similar format and detail as iFixit's [iPhone x Teardown](https://www.ifixit.com/Teardown/iPhone+X+Teardown/98975) and [iPhone X Battery Replacement](https://www.ifixit.com/Guide/iPhone+X+Battery+Replacement/103390) guides including *Tools, Parts (Bill of Materials (BOM)), Introduction, and Steps*. You are permitted and encouraged to use any resources available for help identifying components (including this very neat [infographic](https://www.dronefly.com/the-anatomy-of-a-drone) from Dronefly and this [presentation](https://catsr.vse.gmu.edu/SYST460/DroneComponents.pdf) from George Mason University). The team's teardown instructions will be graded as follows:

| \#  | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | Points |
|:---:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |:------:|
| 1.  | **Introduction**<br/>- Does the guide include a strong introduction paragraph?<br/>- Contain relevant background information?<br/>- Special requirements? Hazards?<br/>- Table of contents                                                                                                                                                                                                                                                                                                                                                              | 2      |
| 2.  | **Tools and Parts**<br/>- Does the guide list the required tools?<br/>- Tool sizes?<br/>- Are tools listed using [task list formatting](https://docs.gitlab.com/ee/user/markdown.html#task-lists)?<br/>- Does parts list include sufficient information (model, description, part number, and quantity)?                                                                                                                                                                                                                                                | 2      |
| 3.  | **Instructions**<br/>- Are instructions concise?<br/>- Sufficient detail?<br/>- Clear description of tools used in each step?<br/>- Clear description of action taken to remove part?<br/>- Quality of photos?<br/>- Photo markup for clarity?<br/>- [Admonitions/Blockquotes](https://docs.gitlab.com/ee/user/markdown.html#blockquotes) to call attention to important steps where appropriate?[^1]<br/>- Diagrams (wiring, mounting, etc.)<br/>- [Optional] [flow charts](https://docs.gitlab.com/ee/user/markdown.html#mermaid) to show connections | 5      |
| 4.  | **Writing**<br/>- Do the steps follow logical order?<br/>- Is it concise?<br/>- Is it intelligible to a wide audience?<br/>- Does the formatting permit easy viewing and navigating?<br/>- Are photo captions[^2] accurate?                                                                                                                                                                                                                                                                                                                             | 3      |
| 5.  | **File Structure**<br/>- Are photos stored in an `img` folder and correctly referenced?<br/>- Are descriptive file names used for photos and other supplemental files?                                                                                                                                                                                                                                                                                                                                                                                  | 3      |

### 4.4 Data Collection (20 pts.)

Throughout the reverse engineering process it is very important to collect measurements of the UAS to understand the weight, balance, and performance of the UAS. In lecture next week, we will make use of the data that you collect to calculate the performance of the UAS under difference conditions using [xcopterCalc](https://www.ecalc.ch/xcoptercalc.php). 

Through the process of collecting the following data, **fully** disassemble the UAS and be sure to log and organize all parts (screws, wires, flight controller, etc.) in the provided organizer. 

#### 4.4.1. Balance (3 pts.)

##### UAS Balance

Because a multi-rotor uses a flight controller to perform stabilized flight, it *appears* immune to weight imbalances. However, if the multi-rotor is unbalanced, the flight controller will send more power to the heavy-side motors. Because the performance of the propulsion system (rotors, motors, ESCs) is finite, distributing power unevenly to maintain level flight decreases the overall performance of the UAS. That is, if a front-heavy quad-rotor UAS is maintaining hover at 50% total throttle, but the front motors are outputting 65% of the total power and the aft motors are outputting 35% of the total power, then the system is limited to output a maximum thrust of 85% while still remaining level. Review *[Figure 1](#fig1)* showing three UAS each with maximum thrust outputs of 100%, 85%, and 65% respectively.

![lab3_cgAll](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab3_cgAll.png)*<a name="fig1">Figure 1: Movement of CG causes heavy-side motors to work harder to maintain steady and level flight limiting the available total thrust.</a>*

Extend the UAS arms fully and prepare the UAS as if for flight. Using a ruler, measure and mark the expected center point of the UAS. Next, using the CG Balancer tool, measure the balance of the UAS at the marked center point. Complete the following table using \#0 as an example of a UAS with the CG 6 mm front of the center point, but otherwise centered left to right. Move the drone until it is balanced. \*If you are using a UAS without a full set of props, it's expected that your CG will be farther from the center point - include information about missing pieces affecting the CG in a note below the table. If your CG is truly in the center, make note of that.

:earth_americas: Complete the table below:

| #   | Egocentric direction from center (front to back and left to right) | Measurement from center (mm) |
|:---:| ------------------------------------------------------------------ | ---------------------------- |
| 0.  | *e.g., Front*                                                      | *e.g., 6 mm*                 |
| 1.  | `          `                                                       | `          `                 |
| 2.  | `          `                                                       | `          `                 |

##### Propeller Balance

Recall from lecture 2.2 that unbalanced propeller can cause vibrations, premature motor wear, and instability. Balancing propeller can be accomplished using a propeller balancer like the [Top Flite Precision Magnetic Balancer](https://www.horizonhobby.com/product/power-point-propeller-balancer/TOPQ5700.html). Review the "HOW TO USE YOUR TOP FLITE PRECISION MAGNETIC BALANCER" section of the [instruction manual](https://gitlab.com/purdue-uas/at21900/course-material/-/blob/main/resources/topq5700-manual.pdf) and balance one propeller per group member. Use either sand paper or tape to adjust the material mass on the heavier or lighter blade of the propeller respectively. If the propeller is truly balanced, it will remain stationary in *any* position. However, if it does not remain stationary, then the hub of the propeller should be balanced. View this [video by Flite Test](https://www.youtube.com/watch?v=4_fL_QuMkp8) for quick tips on balancing the hub, adding weight to the light side of the hub using tape (**Never** remove material from the hub - this will weaken its structural integrity). Take a picture of your propeller in the balancer perfectly balanced. Name it `lab3_PropBalance_FirstLast_YYYYMMDD`, add it to the `img` folder that you cloned, and link to each team member's photo below:

> :thought_balloon: **Remember:** You can link to a picture using the following formatting:
> 
> `![picture description](img/nameOfFile.extension)`

#### 4.4.2. Weight (3 pts)

Weight is crucial to calculating the performance of a UAS. Weight not only affects performance, but also stability. A drone that hovers at 50% throttle has 50% more throttle to deal with perturbations caused by wind or other external forces. However, a the same drone with a heavy payload attached to it might hover at 75% throttle leaving only 25% more throttle to deal with perturbations to the system. To compensate for more weight, the motors, ESCs, propellers, or any combinations should be adjusted to meet the performance needs to hover close to 50% throttle. 

:earth_americas: Record or calculate the following weights:

| #   | Item Description                                                                                                           | Weight (g)   |
|:---:| -------------------------------------------------------------------------------------------------------------------------- | ------------ |
| 1.  | [Zero-fuel weight (ZFW)](https://en.wikipedia.org/wiki/Zero-fuel_weight)                                                   | `          ` |
| 2.  | [Aircraft gross weight (AUW)](https://en.wikipedia.org/wiki/Zero-fuel_weight)<br/>- Including battery (fuel), props., etc. | `          ` |
| 3.  | Battery                                                                                                                    | `          ` |
| 4.  | Flight Controller                                                                                                          | `          ` |
| 5.  | GPS Unit                                                                                                                   | `          ` |
| 6.  | Total (4) Motors <br/>- Excluding any mounting hardware                                                                    | `          ` |
| 7.  | Total (4) ESCs                                                                                                             | `          ` |
| 8.  | Empty Compartment Organizer                                                                                                | `          ` |
| 9.  | Compartment Organizer<br/>- Including *all* mounting hardware                                                              | `          ` |

#### 4.4.3. Dimensions (1 pt.)

In order to optimize UAS performance, it is important to pair the frame with an appropriately sized powertrain. 

:earth_americas: Record or calculate the following dimensions:

| #   | Item Description                                                               | Dimensions (mm) |
|:---:| ------------------------------------------------------------------------------ | --------------- |
| 1.  | Frame size<br/>- Measure from opposite-corner motors. See *[Figure 2](#fig2)*. | `          `    |

![lab3_frameDimensions](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab3_frameDimensions.png)<a name="fig2">*Figure 2: Measure the UAS frame side from opposite-corner motors.*</a>

#### 4.4.4. Motor Specs. (2 pts.)

There are two main types of motors that can be used on multi-rotor UAS, brushed DC motors and brushless DC motors. Brushless DC (BLDC) motors are a newer technology and have become the industry standard as they are more efficient and durable. However, brushed motors continue to be used in low-cost toy drones for their cost effectiveness. Motors will be covered in more depth in lecture, so for now it is most important to identify the parts of a motor as well as metrics used to measure a motor's performance. For this sections your team should disassemble a motor to identify the components. To disassemble, simply remove the small retaining ring on the bottom of the motor and the motor should come apart into two pieces, the stator and the rotor/bell. Take a picture of the stator and the rotor separately and link either the corresponding term below. 

> :thought_balloon: **Remember:** You can link to a picture using the following formatting:
> 
> `![picture description](img/nameOfFile.extension)`

:earth_americas: Define the following:

**Stator (include photo) -** `          `

**Rotor/bell (include photo) -** `          `

**Kv Rating -** `          `

:earth_americas: Record the following from the motor and/or it's data sheet:

| #   | Item Description    | Value        |
|:---:| ------------------- | ------------ |
| 1.  | Kv Rating (rpm/v)   | `          ` |
| 2.  | Stator Width (mm)   | `          ` |
| 3.  | Stator Heigh (mm)   | `          ` |
| 4.  | Maximum Power (W)   | `          ` |
| 5.  | Maximum Current (A) | `          ` |
| 6.  | Suitable LiPo (s)   | `          ` |

#### 4.4.5. ESC Specs. (2 pts.)

The electronic speed controller (ESC) is critical in translating a low voltage signal from the flight controller to a high-power output to the motors. Additionally, the ESC provides power to the flight controller through a battery eliminator circuit (BEC).

:earth_africa: Complete the blanks in the following table identifying the ESC connections and their function:

| Connection Type | Wire Color        | Function       |
|:---------------:|:-----------------:|:--------------:|
| DC Power        | Red               | `          `   |
| -               | `          `      | Ground         |
| -               | -                 | -              |
| `          `    | Blue/Red/Other    | `          `   |
| -               | Blue/Yellow/Other | `          `   |
| -               | Blue/Black/Other  | `          `   |
| -               | -                 | -              |
| Servo Connector | `          `      | Throttle Input |
| -               | Red               | `          `   |
| -               | Black             | `          `   |

:earth_americas: Additionally, record the specifications for the ESC in the table below:

| #   | Item Description | Value        |
|:---:|:----------------:|:------------:|
| 1.  | Manufacturer     | `          ` |
| 2.  | Model            | `          ` |
| 3.  | Constant Current | `          ` |
| 4.  | Burst Current    | `          ` |
| 5.  | Suitable LiPo    | `          ` |
| 6.  | BEC Voltage      | `          ` |
| 7.  | BEC Max Current  | `          ` |

:earth_americas: Finally, *carefully* slice the heat shrink on one (1) ESC revealing the circuit board below. Take a photo of both sides and attach them below.

> :thought_balloon: **Remember:** You can link to a picture using the following formatting:
> 
> `![picture description](img/nameOfFile.extension)`

#### 4.4.6. Propeller Specs. (2 pts.)

Propellers are labeled by their length and pitch at a minimum. Often they are also labeled with their number of blades and direction of rotation. 

:earth_africa: First we need to understand pitch. Fill in the blanks: 

Pitch is defined by the `          ` of the blade/propeller. A higher (coarser) pitch propeller will take a `          ` bite out of the air with each turn and will move through the air a `          ` distance with each revolution. However, a lower (finer) pitch propeller will take a `          ` bite out of the air with each turn and will move through the air a `          ` distance with each revolution.  

:earth_asia: Complete the following chart for one of your props.:

| #   | Item Description      | Value (include units) |
|:---:|:---------------------:|:---------------------:|
| 1.  | Prop. Length          | `          `          |
| 2.  | Prop. Pitch           | `          `          |
| 3.  | Direction of Rotation | `          `          |
| 4.  | Material              | `          `          |

#### 4.4.7. Battery Specs. (1 pt.)

The battery is one of the heaviest components of the UAS and should be sized appropriately to maximize flight time. 

:earth_asia: Using the suitable battery measurements from sections [4. Motor Specs.](#444-motor-specs-2-pts) and [5. ESC Specs.](#445-esc-specs-2-pts) what is the maximum cell count (S) that this UAS can take? `          `

:earth_americas: Complete the table below for the provided battery:

| #   | Item Description       | Value        |
|:---:|:----------------------:|:------------:|
| 1.  | Chemistry              | `          ` |
| 2.  | Cell Count (S)         | `          ` |
| 3.  | Nominal Voltage (V)    | `          ` |
| 4.  | Capacity (mAh)         | `          ` |
| 5.  | Discharge Rating (C)   | `          ` |
| 6.  | Safest Charge Rate (C) | `          ` |

#### 4.4.8. Wiring Diagram and Table (6 pts.)

The wiring diagram will most likely be easiest to complete on a sheet of printer paper. Draw and describe every connection on the UAS.

:earth_americas: Upload picture of diagram below.

> :thought_balloon: **Remember:** You can link to a picture using the following formatting:
> 
> `![picture description](img/nameOfFile.extension)`

In addition to drawing the diagram, complete the table below to show the connections for each component - add columns and rows as required.\*

| #   | Component    | Wire Color   | Signal       | Termination 1 | Termination 2 |
|:---:|:------------:|:------------:|:------------:|:-------------:|:-------------:|
| 1.  | `          ` | `          ` | `          ` | `          `  | `          `  |
| 2.  | `          ` | `          ` | `          ` | `          `  | `          `  |
| 3.  | `          ` | `          ` | `          ` | `          `  | `          `  |

\* If you are missing components, enter the component in the table, but note that you are missing the part (if you can, make assumptions about how the missing component *would* be wired).

---

[^1]: Admonitions can be used to call attention to particular steps that need the reader's attention. For example:

> :warning: **Caution: Hot Soldering Iron**
> 
> Soldering iron reached temperatures between 200 to 480 °C and can cause severe burns.

[^2]: Captions can be added to your photos in the following format:

```markdown
![Semantic description of image](/images/path/to/folder/image.png)*My caption*
```

:wave: Hey there! Notice anything you would like improved? Suggestions are always welcomed! Please [create a new issue](https://gitlab.com/purdue-uas/at21900/course-material/-/issues) on GitLab or [email](mailto:incoming+purdue-uas-at21900-course-material-32681911-47fahqsg66ijh2ikfun4yji7d-issue@incoming.gitlab.com?subject=[lab3]) your suggestion. 

[![CC BYNCSA 40](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
