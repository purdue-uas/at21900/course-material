# Lab 8 - Assemble Motors, ESCs, Balance Propellers, and Performance Calculations (27 points)

[TOC]

---

## 1. Objectives

The objective of this lab is to complete the assembly of the motors, ESCs, and associated wire harness. The wire assembly will need to meet [14 CFR 25.1703](https://www.ecfr.gov/current/title-14/chapter-I/subchapter-C/part-25/subpart-H/section-25.1703) Function and Installations as well as [14 CFR 25.1711](https://www.ecfr.gov/current/title-14/section-25.1711) Component identification: EWIS. Additionally, the build will be required to carry instructions for continued airworthiness per [14 CFR 25.1729](https://www.ecfr.gov/current/title-14/section-25.1729) and corresponding sections of [Appendix H to Part 25](https://www.ecfr.gov/current/title-14/chapter-I/subchapter-C/part-25/appendix-Appendix%20H%20to%20Part%2025): H25.4 *Airworthiness Limitations section* and H25.5 *Electrical Wiring Interconnection System (EWIS) Instructions for Continued Airworthiness.* Further detail is found in [AC 25.1701-1](https://www.faa.gov/regulations_policies/advisory_circulars/index.cfm/go/document.information/documentID/73476) Certification of Electrical Wiring Interconnection Systems on Transport Category Airplanes.

Additionally, using an the online performance calculator, [eCalc](https://www.ecalc.ch), determine the expected performance calculations for the UAS with different battery combinations.

---

## 2. Overview

This semester we are building a 650 mm frame quadrotor from a kit; However, the techniques that you learn while building a small UAS translate linearly to larger and larger aircraft. That is, upon graduation from the UAS program, you may find yourself working on the scale of micrometers with [drone swarms](https://www.forbes.com/sites/davidhambling/2021/03/01/what-are-drone-swarms-and-why-does-everyone-suddenly-want-one/?sh=47f362ae2f5c), on the scale of [eVTOLs for urban air mobility (UAM)](https://www.jobyaviation.com), or anywhere in between. 

sUAS currently do not have maintenance or manufacturing requirements presenting a huge gap in safety. The trend in sUAS regulation is moving sUAS the way of crewed (manned) aircraft with scheduled maintenance procedures based on manufacturer and other stakeholder guidelines. In addition to supporting the longevity of the aircraft, scheduled maintenance also generates and ongoing dataset of component reliability to determine future maintenance schedule updates. 

Before routing the [wire harness](https://www.interconnect-wiring.com/blog/what-is-a-wiring-harness/) review the following sections from 14 CFR:

- [14 CFR 25.1703](https://www.ecfr.gov/current/title-14/chapter-I/subchapter-C/part-25/subpart-H/section-25.1703) Function and Installations

- [14 CFR 25.1711](https://www.ecfr.gov/current/title-14/section-25.1711) Component identification: EWIS

- [14 CFR 25.1729](https://www.ecfr.gov/current/title-14/section-25.1729) Instructions for Continued Airworthiness: EWIS.

- [Appendix H to Part 25](https://www.ecfr.gov/current/title-14/chapter-I/subchapter-C/part-25/appendix-Appendix%20H%20to%20Part%2025): 
  
  - H25.4 *Airworthiness Limitations section*
  
  - H25.5 *Electrical Wiring Interconnection System (EWIS) Instructions for Continued Airworthiness.*

- [AC 25.1701-1](https://www.faa.gov/regulations_policies/advisory_circulars/index.cfm/go/document.information/documentID/73476) Certification of Electrical Wiring Interconnection Systems on Transport Category Airplanes

## 3. Assembly

### 3.1. Installing Motors (6 points):

The motors should be installed to the frame using the appropriate hardware (refer to team BOM). Ensure that the motors are sufficiently tight while taking care not to strip the heads. Orient the wires to route to the center. 

#### Assignment: Questions

1. What is the maximum tightening torque for the motor screws? `torque range & source`

2. List potential issues with the installation (think: maintenance requirements/checks) below and add to your maintenance checklist:
   
   1. `issues`

#### Assignment: Instructor Inspection

- [ ] Installation screws not stripped `signoff`

- [ ] Installation screws sufficiently tightened `signoff`

- [ ] Wire assembly correctly positioned `signoff`

- [ ] Motor centered `signoff`

### 3.2. Install ESCs (6 points)

Install the ESCs into the arms using the provided zip ties. Ensure that each ESC is positioned such that both lengths of wire are able to reach the terminal locations. 

Install the power distribution board in the center of the frame. This may require disassembly as well as drilling holes in to the center board. If your center board does not have holes, work with the instructor to add them.

#### Assignment: Questions

1. What are possible concerns or potential problems with the power distribution board?
   
   1. `potential problem 1`

2. What maintenance items should be performed on the ESCs (list below as well as in maintenance checklist):
   
   1. `maintenance item 1`

3. How does your installation meet the guidelines of [AC 25.1701-1 Section 25.1703](https://www.faa.gov/documentLibrary/media/Advisory_Circular/AC_25_1701-1.pdf)?
   
   1. `guideline 1`

#### Assignment: Instructor Inspection

- [ ] Verify security of zip tie `signoff`

- [ ] Inspect power distribution board soldering

- [ ] Inspect heatshrink installation

### 3.3. Balance Propellers (4 points)

Review the techniques in [Lab 3](https://gitlab.com/purdue-uas/at21900/course-material/-/tree/main/week3/lab3#propeller-balance) for balancing a propeller. Balance both the propeller blades as well as the hub.

Do not install the propellers on the motors. 

#### Assignment: Questions

1. Why is it important to balance the propellers?

2. Why does the hub balance separate from the blades?

#### Assignment: Instructor Inspection

- [ ] Verify blade balance `signoff`

- [ ] Verify hub balance `signoff`

## 4. Performance Calculations

Understanding the performance of the UAS is crucial to the iterative build process. We are starting out with a generic kit meaning that we know it will fly, but we do not know how *well* it will fly. 

### 4.1. Standard Performance (5 points)

#### Assignment

Record all of the data required for calculations and input into the table below:

| Item      | Value          |
| --------- |:--------------:|
| Propeller | APC SlowFly SF |
|           |                |
|           |                |
|           |                |

Using [eCalc](https://www.ecalc.ch) (username: `at219`, password: `purdue`), calculate and complete the following table:

| Performance Metric           | Value |
| ---------------------------- |:-----:|
| Hover Flight Time            |       |
| Thrust-Weight                |       |
| Battery Load                 |       |
| Motor Electric Power @ Hover |       |
|                              |       |

Additionally, document these values in your Operations Manual document under a header titled "Calculated Performance". The flight performance characteristics will later be tested in a real-world application and be listed under a header titled "Measured Performance"

### 4.2. Alternative Configuration Performance (6 points)

#### Assignment: Parameter deviations from standard (2/6 points)

Again, using [eCalc](https://www.ecalc.ch) (username: `at219`, password: `purdue`), run and record the following calculations (Be sure to return the changed value back to standard between calculations so that you are performing the calculation as a change from the standard setup):

| Category     | Item            | New Value           | Hover Flight Time* | Thrust-Weight* |
| ------------ | --------------- | ------------------- | ------------------ | -------------- |
| General      | Field Elevation | 0m ASL              |                    |                |
| General      | Field Elevation | 1000m ASL           |                    |                |
| General      | Air Temperature | -10°C               |                    |                |
| General      | Air Temperature | 38°C                |                    |                |
| Battery Cell | Type            | 10000mAh            |                    |                |
| Motor        | Model           | T-Motor P2207.51750 |                    |                |

*List these values as follows: `new calculated [change from standard calculation]` for example: `26.1 min [-1.2 min]`

#### Assignment: Questions (1/6 points):

1. Compare the flight characteristics you would expect by replacing the stock motors with the T-Motor in the table above. 
   
   `flight characteristics`

#### Assignment: Convert to Octocopter (2/6 points)

Complete the following steps to convert your UAS from a quadcopter to an octocopter.

1. Setup your quad to use the standard settings in [section 4.1](#41-standard-performance-5-points) above. 

2. Using the Prop-Kv wizard set the propeller size to maximum, but keep the pitch the same. Click Calculate.

3. Find a motor that will work with the new prop, choose the lightest motor.

4. Recalculate and fill in the table below.

5. Using the component-level weight data from your team's lab 3 document, calculate the total weight of converting the quadcopter into an octocopter and record the new weight in the table below.

6. Using the Prop-Kv wizard set the propeller size to maximum, but keep the pitch the same. Click Calculate.

7. Find a motor that will work with the new prop, choose the lightest motor.

8. Recalculate and complete the table below.

| Setup | Category    | Item                | Value |
| ----- | ----------- | ------------------- | ----- |
| Quad  | General     | Model Weight        |       |
| Quad  | Motor       | Manufacture & Model |       |
| Quad  | Propeller   | Diameter (inch)     |       |
| Quad  | Calculation | Hover Flight Time   |       |
| Quad  | Calculation | Thrust-Weight       |       |
| Octo  | General     | Model Weight        |       |
| Octo  | Motor       | Manufacture & Model |       |
| Octo  | Propeller   | Diameter (inch)     |       |
| Octo  | Calculation | Hover Flight Time   |       |
| Octo  | Calculation | Thrust-Weight       |       |

#### Assignment: Questions (1/6 points):

1. What applications might you choose the octocopter over the quadcopter?
   
   `octo > quad`

## 5. Wrapping up

### 5.1. Documentation

Continue to make improvement to your documentation to include and record all available data (performance, assembly, maintenance, schedule, flight hours, etc.). The final assignment will require a completed **Operations Manual** including lab member contribution. It is each team member's responsibility to record your contribution and add it to the document. 

Link to your documentation below:

`link`

### 5.2. Turn in

Turn in your completed lab either by pushing to GitLab or uploading to BrightSpace

---

:wave: Hey there! Notice anything you would like improved? Suggestions are always welcomed! Please [create a new issue](https://gitlab.com/purdue-uas/at21900/course-material/-/issues) on GitLab or [email](mailto:incoming+purdue-uas-at21900-course-material-32681911-47fahqsg66ijh2ikfun4yji7d-issue@incoming.gitlab.com?subject=[lab3]) your suggestion. 

[![CC BYNCSA 40](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
