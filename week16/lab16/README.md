# Lab 16 - Vibration Analysis, Course Review, and Documentation (20 points)

[TOC]

---

## 1. Objectives

The objective of this lab is to perform a vibration analysis from a UAS flight data log. Additionally, this lab will have each individual write a short course review.

---

## 2. Vibration Analysis

Vibration is the enemy of good flight performance. Recall that vibration from the motors, propellers, winds, etc. travel into the accelerometers and gyroscopes. These sensors, parts of the inertial measurement unit, are highly sensitive to vibrations. While  some modern flight controllers have build in vibration damping, many do not in order to reduce size and weight leaving the vibration damping up to the manufacturer. 

Because vibrations are so prolific, particularly in smaller frame aircraft, the ArduPilot firmware has a built in analysis tool. 

### 2.1. Review ArduPilot Documents

Read the [Vibe Dataflash Log Message ArduPilot document](https://ardupilot.org/copter/docs/common-measuring-vibration.html#vibe-dataflash-log-message) and answer the following question: 

If you plot the X, Y, and Z-axes of the accelerometers and notice a spike in the Z axis, what is one possible reason? Why?

### 2.2. Download Data Log

First, download a data log from a recent flight on your UAS. The instructions for downloading data logs with MissionPlanner is available [here](https://ardupilot.org/copter/docs/common-downloading-and-analyzing-data-logs-in-mission-planner.html#common-downloading-and-analyzing-data-logs-in-mission-planner-downloading-logs-via-mavlink) and the instructions for QGroundControl is available [here]([Analyze · QGroundControl User Guide](https://docs.qgroundcontrol.com/master/en/analyze_view/)) and [here](https://docs.qgroundcontrol.com/master/en/analyze_view/log_download.html).

### 2.3. Plot CLIP Values

Open the [ArduPilot online log viewer](https://plot.ardupilot.org/#/) and open the bin file that was just downloaded. (*[Figure 1](#fig1)*)

*<a name="fig1">![lab16_openLog](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab16_openLog.png) <br>Figure 1: Open log file in [ArduPilot Log Viewer](https://plot.ardupilot.org/#/)</a>*

Plot Clip0, Clip1, Clip2 located under the VIBE/[#] headings. Inlcude a screenshot of this plot in your documentation and annotate the graph to answer what the plotted values mean. (hint: review the docs!) (*[Figure 2](#fig2)*)

*<a name="fig2">![lab16_clipPlot](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab16_clipPlot.png) <br>Figure 2: Example plot of Clip0, Clip1, and Clip2</a>*

### 2.4. Plot VIBE Values

Open the [ArduPilot online log viewer](https://plot.ardupilot.org/#/) and open the bin file that was just downloaded.

Plot VibeX, VibeY, and VibeZ located under the VIBE\[#] headings. Inlcude a screenshot of this plot in your documentation and annotate the graph to answer what the plotted values mean. Can you identify any spikes? If so, what do they mean? Are there any improvements that you could make in your vibration damping mount? (*[Figure 3](#fig3)*)

## 3. Course Review

Please take a moment to review this course. I would like for each person in your group to write a short review of this course including answers to the following:
1. What generally worked well in the course?
2. What did the instructor do that helped you achieve in this course?
3. What is your main take away from this course?
4. What do you wish you learned more of?
5. What could the instructor improve on?
6. What would you add to the next revision of the course?
7. What woudl you like removed from the next revision of the course?
8. What did you think of your team assignments (generally)?

## Documentation

Email documentation to [Nathan](mailto:nathanrose@purdue.edu) ASAP.

---

:wave: Hey there! Notice anything you would like improved? Suggestions are always welcomed! Please [create a new issue](https://gitlab.com/purdue-uas/at21900/course-material/-/issues) on GitLab or [email](mailto:incoming+purdue-uas-at21900-course-material-32681911-47fahqsg66ijh2ikfun4yji7d-issue@incoming.gitlab.com?subject=[lab11]) your suggestion. 

[![CC BYNCSA 40](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
