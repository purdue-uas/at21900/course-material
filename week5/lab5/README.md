# Lab 5 - Complete Phase 3 of Lab 3

## Lab 3 available [here](https://gitlab.com/purdue-uas/at21900/course-material/-/blob/main/week3/lab3/README.md)

In lab this week, we will complete our disassembly and begin to finalize the teardown documentation due one week after lab.



:wave: Hey there! Notice anything you would like improved? Suggestions are always welcomed! Please [create a new issue](https://gitlab.com/purdue-uas/at21900/course-material/-/issues) on GitLab or [email](mailto:incoming+purdue-uas-at21900-course-material-32681911-47fahqsg66ijh2ikfun4yji7d-issue@incoming.gitlab.com?subject=[lab3]) your suggestion. 

[![CC BYNCSA 40](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
