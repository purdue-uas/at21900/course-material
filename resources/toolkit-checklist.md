# Toolkit Checklist

Remember to include your toolkit ID (labeled on the side of each toolkit) in every lab assignment. Inclusion of toolkit ID in lab assignment validates completion of this checklist. Failure to report toolkit ID, missing tools, additional tools, or broken tools will result in point reduction. 

## Checklist[^1]

Below is a list of all of the tools included in the toolkit. Verify the toolkit at both checkout and checkin. 

- [ ] [Red "Westward" Brand Toolbox:](#red-westward-brand-toolbox)
  
  - [ ] [Upper Level:](#upper-level)
    
    - [ ] [T-Handled Hex Wrench Set (6 pcs.):](#t-handled-hex-wrench-set-6-pcs)
      
      - [ ] 2 mm
      
      - [ ] 2.5 mm
      
      - [ ] 3 mm
      
      - [ ] 4 mm
      
      - [ ] 5 mm
      
      - [ ] 6 mm
    
    - [ ] [Scissors](#scissors)
  
  - [ ] [Lower Level:](#lower-level)
    
    - [ ] [Open-End Wrench Set (8 pcs.):](#open-end-wrench-set-8-pcs)
      
      - [ ] 5.0 mm
      
      - [ ] 5.5 mm
      
      - [ ] 6.0 mm
      
      - [ ] 7.0 mm
      
      - [ ] 8.0 mm
      
      - [ ] 9.0 mm
      
      - [ ] 10.0 mm
      
      - [ ] 11.0 mm
    
    - [ ] [CG Balancer](#cg-balancer)
    
    - [ ] [Light Duty Utility Knife](#light-duty-utility-knife)
    
    - [ ] [Needle Nose Pliers](#needle-nose-pliers)
    
    - [ ] [Wire Flush Cutters](#wire-flush-cutters)
    
    - [ ] [Nut Driver Set](#nut-driver-set)
      
      - [ ] 4.0 mm
      
      - [ ] 5.5 mm
      
      - [ ] 7.0 mm
      
      - [ ] 8.0 mm
    
    - [ ] [Multi Tool - Stripper/Crimper/Wire Cutter](#multi-tool-strippercrimperwire-cutter)
    
    - [ ] [Screwdriver Set w/ Zipper Pouch (8 Pcs.)](#screwdriver-set-w-zipper-pouch-8-pcs)
      
      - [ ] 5/64" Slotted
      
      - [ ] 3/32" "Slotted
      
      - [ ] 1/8" Slotted
      
      - [ ] 5/32" Slotted
      
      - [ ] \#00 Phillips
      
      - [ ] \#0 Phillips
      
      - [ ] \#1 Phillips
    
    - [ ] [Multimeter](#multimeter)

## Tool Photos

### Red "Westward" Brand Toolbox:

![tools_toolbox](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/tools_toolbox.jpg)

#### Upper Level:

![tools_toolboxUpper](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/tools_toolboxUpper.jpg)

##### T-Handled Hex Wrench Set (6 pcs.):

![tools_hexWrenchSet](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/tools_hexWrenchSet.jpg)

##### Scissors:

![tools_scissors](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/tools_scissors.jpg)

---

#### Lower Level:

![tools_toolboxLower](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/tools_toolboxLower.jpg)

##### Open-End Wrench Set (8 pcs.):

![tools_open-endWrenchSet](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/tools_open-endWrenchSet.jpg)

##### CG Balancer:

![tools_cgBalancer](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/tools_cgBalancer.jpg)

##### Light Duty Utility Knife:

![tools_lightDutyKnife](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/tools_lightDutyKnife.jpg)

##### Needle Nose Pliers:

![tools_needleNosePliers](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/tools_needleNosePliers.jpg)

##### Wire Flush Cutters:

![tools_wireFlushCutters](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/tools_wireFlushCutters.jpg)

##### Nut Driver Set:

![tools_nutDriverSet](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/tools_nutDriverSet.jpg)

##### Multi Tool - Stripper/Crimper/Wire Cutter:

![tools_multiTool](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/tools_multiTool.jpg)

##### Screwdriver Set w/ Zipper Pouch (8 Pcs.):

![tools_screwdriverSet](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/tools_screwdriverSet.jpg)

##### Multimeter

![tools_multimeter](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/tools_multimeter.jpg)

[^1]: Checklist subject to changes and updates. Always check for latest updates.
