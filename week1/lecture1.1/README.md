<img title="" src="https://gitlab.com/at21900/course-material/-/raw/main/img/purdue_logo.png" alt="Purdue SATT Logo" data-align="center">

# Lecture 0.1 - Introduction and Course Overview

This module will introduce the following topics:

- [ ] Review the syllabus and expectations

- [ ] Begin learning about documentation

- [ ] Start thinking about why building custom drones is important

- [ ] 

[![CC BY-NC-SA 4.0](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
